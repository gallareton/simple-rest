<?php

namespace GallREST\Resource;

use Exception;

class Curl implements ResourceInterface
{
    /** @var resource */
    protected $handle;

    /** @var array */
    private $responseHeaders = [];


    public function init($url = null): void
    {
        $this->assertHandleEnabled(false);
        $this->handle = curl_init();
        if (null !== $url) {
            $this->setUrl($url);
        }
        $this->setOption(CURLOPT_RETURNTRANSFER, 1);
        $this->setOption(CURLOPT_HEADERFUNCTION,
            function ($curl, $header) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) {
                    return $len;
                }

                $this->responseHeaders[strtolower(trim($header[0]))][] = trim($header[1]);

                return $len;
            }
        );
    }


    public function setOption($name, $value): void
    {
        $this->assertHandleEnabled();
        curl_setopt($this->handle, $name, $value);
    }


    public function execute(): string
    {
        $this->assertHandleEnabled();
        return curl_exec($this->handle);
    }


    public function close(): void
    {
        $this->assertHandleEnabled();
        curl_close($this->handle);
    }


    public function isOpen(): bool
    {
        if (is_resource($this->handle)) {
            return true;
        }

        return false;
    }


    public function getInfo(): array
    {
        $this->assertHandleEnabled();
        return curl_getinfo($this->handle);
    }


    public function setHeaders(array $headers): void
    {
        $this->assertHandleEnabled();
        $this->setOption(CURLOPT_HTTPHEADER, $headers);
    }


    public function setUrl(string $url): void
    {
        $this->assertHandleEnabled();
        $this->setOption(CURLOPT_URL, $url);
    }


    public function setMethod(string $method): void
    {
        $this->assertHandleEnabled();
        $this->setOption(CURLOPT_CUSTOMREQUEST, strtoupper($method));
        if (strtoupper($method) === 'POST') {
            $this->setPost(true);
        }
    }


    public function setPostFields(array $postFields): void
    {
        $this->assertHandleEnabled();
        $this->setOption(CURLOPT_POSTFIELDS, $postFields);
    }


    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }


    private function setPost(bool $post): void
    {
        $this->assertHandleEnabled();
        $this->setOption(CURLOPT_POST, $post);
    }


    /**
     * @param bool $enabled
     * @throws Exception
     */
    private function assertHandleEnabled($enabled = true)
    {
        if ($enabled && !$this->isOpen()) {
            throw new Exception(
                'Curl not initialized. Did you call Curl::init()?'
            );
        }

        if (!$enabled && $this->isOpen()) {
            throw new Exception(
                'Curl already initialized. Did you call Curl::close()?'
            );
        }
    }
}