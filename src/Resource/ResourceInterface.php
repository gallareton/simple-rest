<?php

namespace GallREST\Resource;


interface ResourceInterface
{
    public function init($url = null): void;


    public function setOption($name, $value): void;


    public function execute(): string;


    public function close(): void;


    public function isOpen(): bool;


    public function getInfo(): array;


    public function setHeaders(array $headers): void;


    public function setUrl(string $url): void;


    public function setMethod(string $method): void;


    public function setPostFields(array $postFields): void;


    public function getResponseHeaders(): array;
}