<?php

namespace GallREST\Tests;


use GallREST\Client;
use PHPUnit\Framework\TestCase;

class ClientIntegrationTest extends TestCase
{
    const URL = 'https://reqres.in';
    /**
     * @var Client
     */
    private $client;


    protected function setUp(): void
    {
        $this->client = new Client();
    }


    public function testShouldCreateUser(): void
    {
        $endpoint = '/api/users';
        $response = $this->client->request(
            'POST',
            self::URL . $endpoint,
            [
                'name' => 'gall',
                'job' => 'commander',
            ],
            [],
            [CURLOPT_SSL_VERIFYPEER => 0]
        );
        $content = json_decode($response->getBody(), true);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('createdAt', $content);
        $this->assertIsNumeric($content['id']);
    }


    public function testShouldFetchUser(): void
    {
        $endpoint = '/api/users/2';
        $response = $this->client->request(
            'GET',
            self::URL . $endpoint,
            [],
            [],
            [CURLOPT_SSL_VERIFYPEER => 0]
        );
        $content = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('id', $content['data']);
        $this->assertArrayHasKey('email', $content['data']);
        $this->assertArrayHasKey('first_name', $content['data']);
        $this->assertArrayHasKey('last_name', $content['data']);
        $this->assertArrayHasKey('avatar', $content['data']);
        $this->assertEquals(2, $content['data']['id']);
    }


    public function testShouldDeleteUser(): void
    {
        $endpoint = '/api/users/2';
        $response = $this->client->request(
            'DELETE',
            self::URL . $endpoint,
            [],
            [],
            [CURLOPT_SSL_VERIFYPEER => 0]
        );
        $this->assertEquals(204, $response->getStatusCode());
    }

}