<?php

namespace GallREST\Tests\Resource;

use GallREST\Resource\Curl;
use PHPUnit\Framework\TestCase;

class CurlTest extends TestCase
{

    /**
     * @var Curl
     */
    private $resource;


    protected function setUp(): void
    {
        // Given
        $this->resource = new Curl();
    }


    public function testShouldGetClosedResourceBeforeInitializingResource(): void
    {
        // Then
        $this->assertFalse($this->resource->isOpen());
    }


    public function testShouldThrowExceptionWhenSettingOptionBeforeInitializingResource(): void
    {
        // Expect
        $this->expectExceptionMessage('Curl not initialized. Did you call Curl::init()?');

        // When
        $this->resource->execute();
    }


    public function testShouldGetOpenResourceAfterInitWithEmptyUrl(): void
    {
        // When
        $this->resource->init();

        // Then
        $this->assertTrue($this->resource->isOpen());
        $this->assertEmpty($this->resource->getInfo()['url']);
    }


    public function testShouldThrowExceptionWhenInitGivenAlreadyInitialized(): void
    {
        // Expect
        $this->expectExceptionMessage('Curl already initialized. Did you call Curl::close()?');

        // When
        $this->resource->init();
        $this->resource->init();
    }


    public function testShouldGetOpenResourceWithUrlAfterInitWithUrl(): void
    {
        // Given
        $url = 'test';

        // When
        $this->resource->init($url);

        // Then
        $this->assertTrue($this->resource->isOpen());
        $this->assertSame($url, $this->resource->getInfo()['url']);
    }


    public function testShouldGetOpenResourceWithUrlAfterInitWithEmptyUrlAndParametersSetAfter(): void
    {
        // Given
        $url = 'test';

        // When
        $this->resource->init();
        $this->resource->setUrl($url);
        $this->resource->setMethod('POST');
        $this->resource->setPostFields(['test' => 'sample']);

        // Then
        $this->assertTrue($this->resource->isOpen());
        $this->assertSame($url, $this->resource->getInfo()['url']);
    }


    public function testShouldReturnEmptyResponseGivenNoOptionsProvided(): void
    {
        // When
        $this->resource->init();

        // Then
        $this->assertTrue($this->resource->isOpen());
        $this->assertEmpty($this->resource->execute());
    }


    public function testShouldCloseAndReinitializeResource(): void
    {
        // When
        $this->resource->init();
        $this->resource->close();
        $this->resource->init();

        // Then
        $this->assertTrue($this->resource->isOpen());
    }


    public function testShouldReceiveEmptyHeaders()
    {
        //When
        $this->resource->init();

        //Then
        $this->assertEmpty($this->resource->getResponseHeaders());
    }


    protected function tearDown(): void
    {
        if ($this->resource->isOpen()) {
            $this->resource->close();
        }
    }
}
