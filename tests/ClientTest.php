<?php

namespace GallREST\Tests;


use GallREST\Client;
use GallREST\Resource\Curl;
use GallREST\Resource\ResourceInterface;
use Http\Message\ResponseFactory;
use Nyholm\Psr7\Factory\HttplugFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ClientTest extends TestCase
{

    public function testShouldInitializeClientWithDefaultResourceAndResponseFactory(): void
    {
        // When
        $client = new Client();

        // Then
        $this->assertInstanceOf(Curl::class, $client->getResource());
        $this->assertInstanceOf(HttplugFactory::class, $client->getResponseFactory());
    }


    public function testShouldInitializeClientWithMockResourceAndResponseFactory(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $responseFactory = $this->createMock(ResponseFactory::class);

        // When
        $client = new Client($resource, $responseFactory);

        // Then
        $this->assertSame($resource, $client->getResource());
        $this->assertSame($responseFactory, $client->getResponseFactory());
    }


    public function testShouldMakeRequestAndReceiveResponse(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $responseText = 'OK';
        $data = ['test' => 'sample'];
        $url = 'test';
        $info = [
            'http_code' => 200
        ];
        $resource->expects($this->once())->method('init');
        $resource->expects($this->once())->method('execute')->willReturn($responseText);
        $resource->expects($this->once())->method('getInfo')->willReturn($info);
        $resource->expects($this->once())->method('setUrl')->with($url . '?test=sample');
        $resource->expects($this->never())->method('setMethod');
        $resource->expects($this->never())->method('setPostFields');
        $resource->expects($this->never())->method('setOption');
        $resource->expects($this->once())->method('setHeaders')->with([]);
        $resource->expects($this->once())->method('getResponseHeaders');
        $resource->expects($this->once())->method('close');

        // When
        $client = new Client($resource);
        $response = $client->request('GET', $url, $data);

        // Then
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($responseText, $response->getBody());
        $this->assertEquals($info['http_code'], $response->getStatusCode());
    }


    public function testShouldMakePOSTRequestAndReceive400CodeWithReasonPhrase(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $data = ['test' => 'sample'];
        $url = 'test';
        $reasonPhrase = 'missing data';
        $responseText = json_encode(['error' => $reasonPhrase]);
        $info = [
            'http_code' => 400
        ];
        $resource->expects($this->once())->method('init');
        $resource->expects($this->once())->method('execute')->willReturn($responseText);
        $resource->expects($this->once())->method('getInfo')->willReturn($info);
        $resource->expects($this->once())->method('setUrl')->with($url);
        $resource->expects($this->once())->method('setMethod')->with('POST');
        $resource->expects($this->once())->method('setPostFields')->with($data);
        $resource->expects($this->never())->method('setOption');
        $resource->expects($this->once())->method('setHeaders')->with([]);
        $resource->expects($this->once())->method('getResponseHeaders');
        $resource->expects($this->once())->method('close');

        // When
        $client = new Client($resource);
        $response = $client->request('POST', $url, $data);

        // Then
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($responseText, $response->getBody());
        $this->assertEquals($info['http_code'], $response->getStatusCode());
        $this->assertEquals($reasonPhrase, $response->getReasonPhrase());
    }


    public function testShouldMakePUTRequestAndReceiveResponse(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $data = ['test' => 'sample'];
        $url = 'test';
        $responseText = json_encode($data);
        $info = [
            'http_code' => 200
        ];
        $resource->expects($this->once())->method('init');
        $resource->expects($this->once())->method('execute')->willReturn($responseText);
        $resource->expects($this->once())->method('getInfo')->willReturn($info);
        $resource->expects($this->once())->method('setUrl')->with($url);
        $resource->expects($this->once())->method('setMethod')->with('PUT');
        $resource->expects($this->once())->method('setPostFields')->with($data);
        $resource->expects($this->never())->method('setOption');
        $resource->expects($this->once())->method('setHeaders')->with([]);
        $resource->expects($this->once())->method('getResponseHeaders');
        $resource->expects($this->once())->method('close');

        // When
        $client = new Client($resource);
        $response = $client->request('PUT', $url, $data);

        // Then
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($responseText, $response->getBody());
        $this->assertEquals($info['http_code'], $response->getStatusCode());
    }


    public function testShouldMakeOtherRequestAndReceiveResponse(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $data = ['test' => 'sample'];
        $url = 'test';
        $responseText = '';
        $info = [
            'http_code' => 404,
        ];
        $resource->expects($this->once())->method('init');
        $resource->expects($this->once())->method('execute')->willReturn($responseText);
        $resource->expects($this->once())->method('getInfo')->willReturn($info);
        $resource->expects($this->once())->method('setUrl')->with($url . '?test=sample');
        $resource->expects($this->once())->method('setMethod')->with('DELETE');
        $resource->expects($this->never())->method('setPostFields');
        $resource->expects($this->never())->method('setOption');
        $resource->expects($this->once())->method('setHeaders')->with([]);
        $resource->expects($this->once())->method('getResponseHeaders');
        $resource->expects($this->once())->method('close');

        // When
        $client = new Client($resource);
        $response = $client->request('DELETE', $url, $data);

        // Then
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($responseText, $response->getBody());
        $this->assertEquals($info['http_code'], $response->getStatusCode());
    }


    public function testShouldMakeRequestWithBasicAuthorization(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $credentials = ['test', 'sample'];
        $url = 'test';
        $responseText = '';
        $info = [
            'http_code' => 200,
        ];
        $headers = [];
        $resource->expects($this->once())->method('init');
        $resource->expects($this->once())->method('execute')->willReturn($responseText);
        $resource->expects($this->once())->method('getInfo')->willReturn($info);
        $resource->expects($this->once())->method('setUrl')->with($url);
        $resource->expects($this->once())->method('setMethod')->with('POST');
        $resource->expects($this->once())->method('setPostFields')->with([]);
        $resource->expects($this->never())->method('setOption');
        $resource->expects($this->once())->method('setHeaders')->with(
            [
                sprintf('authorization: basic %s:%s', $credentials[0], $credentials[1]),
            ]
        );
        $resource->expects($this->once())->method('getResponseHeaders');
        $resource->expects($this->once())->method('close');

        // When
        $client = new Client($resource);
        $client->setBasicAuth($credentials[0], $credentials[1]);
        $response = $client->request('POST', $url);

        // Then
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($responseText, $response->getBody());
        $this->assertEquals($info['http_code'], $response->getStatusCode());
    }


    public function testShouldSwitchAuthorization(): void
    {
        // Given
        $resource = $this->createMock(ResourceInterface::class);
        $token = 'TEST';
        $credentials = ['test', 'sample'];
        $url = 'test';
        $responseText = '';
        $info = [
            'http_code' => 200,
        ];
        $resource->expects($this->once())->method('init');
        $resource->expects($this->once())->method('execute')->willReturn($responseText);
        $resource->expects($this->once())->method('getInfo')->willReturn($info);
        $resource->expects($this->once())->method('setUrl')->with($url);
        $resource->expects($this->once())->method('setMethod')->with('POST');
        $resource->expects($this->once())->method('setPostFields')->with([]);
        $resource->expects($this->never())->method('setOption');
        $resource->expects($this->once())->method('setHeaders')->with(
            [
                sprintf('authorization: token %s', $token),
            ]
        );
        $resource->expects($this->once())->method('getResponseHeaders');
        $resource->expects($this->once())->method('close');

        // When
        $client = new Client($resource);
        $client->setBasicAuth($credentials[0], $credentials[1]);
        $client->setToken($token, 'token');
        $response = $client->request('POST', $url);

        // Then
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($responseText, $response->getBody());
        $this->assertEquals($info['http_code'], $response->getStatusCode());
    }

}