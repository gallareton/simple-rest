<?php

namespace GallREST;

use Exception;
use GallREST\Resource\Curl;
use GallREST\Resource\ResourceInterface;
use Http\Message\ResponseFactory;
use Nyholm\Psr7\Factory\HttplugFactory;
use Psr\Http\Message\ResponseInterface;

class Client
{
    const AUTH_NONE = 0;
    const AUTH_BASIC = 1;
    const AUTH_TOKEN = 2;

    /** @var ResourceInterface */
    private $resource;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /** @var int */
    private $authType = self::AUTH_NONE;

    /** @var array */
    private $authData;


    public function __construct(ResourceInterface $resource = null, ResponseFactory $responseFactory = null)
    {
        if (!empty($resource)) {
            $this->setResource($resource);
        } else {
            if (class_exists(Curl::class)) {
                $this->setResource(new Curl());
            }
        }

        if (!empty($responseFactory)) {
            $this->setResponseFactory($responseFactory);
        } else {
            if (class_exists(HttplugFactory::class)) {
                $this->setResponseFactory(new HttplugFactory());
            }
        }
    }


    public function request(
        string $method,
        string $url,
        array $data = [],
        array $headers = [],
        array $options = []
    ): ResponseInterface {
        if (empty($this->resource) || empty($this->responseFactory)) {
            throw new Exception('Resource and ResponseFactory must be set in order to make a request.');
        }

        $resource = $this->getResource();
        $this->initializeResource($resource, $method, $url, $data, $headers, $options);
        $response = $this->getResponse($resource->execute(), $resource->getResponseHeaders(), $resource->getInfo());
        $resource->close();

        return $response;
    }


    public function setToken(string $value, string $headerPrefix = 'bearer'): void
    {
        $this->setAuth(self::AUTH_TOKEN, ['token' => $value, 'prefix' => $headerPrefix]);
    }


    public function setBasicAuth(string $login, string $password): void
    {
        $this->setAuth(self::AUTH_BASIC, ['login' => $login, 'password' => $password]);
    }


    public function getResource(): ResourceInterface
    {
        return $this->resource;
    }


    public function setResource(ResourceInterface $resource): void
    {
        $this->resource = $resource;
    }


    public function getResponseFactory(): ResponseFactory
    {
        return $this->responseFactory;
    }


    public function setResponseFactory(ResponseFactory $responseFactory): void
    {
        $this->responseFactory = $responseFactory;
    }


    protected function setAuth(int $type, array $data)
    {
        if ($type !== self::AUTH_BASIC && $type !== self::AUTH_TOKEN) {
            throw new Exception('Authorization type not supported. Please use AUTH_ constants from Client class.');
        }

        $this->authType = $type;
        $this->authData = $data;
    }


    /**
     * @param ResourceInterface $resource
     * @param string $method
     * @param string $url
     * @param array $data
     * @param array $headers
     * @param array $options
     * @throws Exception
     */
    protected function initializeResource(
        ResourceInterface $resource,
        string $method,
        string $url,
        array $data,
        array $headers,
        array $options
    ): void {
        $resource->init();
        $this->setInitialOptions($resource, $method, $url, $data);
        $this->setHeaders($resource, $headers);
        foreach ($options as $name => $value) {
            $resource->setOption($name, $value);
        }
    }


    /**
     * @param ResourceInterface $resource
     * @param string $method
     * @param string $url
     * @param array $data
     */
    protected function setInitialOptions(ResourceInterface $resource, string $method, string $url, array $data): void
    {
        switch ($method) {
            case 'POST':
                $resource->setMethod('POST');
                $resource->setPostFields($data);
                break;
            case 'PUT':
                $resource->setMethod('PUT');
                $resource->setPostFields($data);
                break;
            default:
                $resource->setMethod($method);
            case 'GET':
                $url = false !== strpos($url, '?') ? $url : sprintf('%s?%s', $url, http_build_query($data));
        }
        $resource->setUrl($url);
    }


    /**
     * @param string $result
     * @param array $headers
     * @param array $info
     * @return ResponseInterface
     */
    protected function getResponse(string $result, array $headers, array $info): ResponseInterface
    {
        $reason = null;
        if ((int)$info['http_code'] >= 300) {
            $decoded = json_decode($result, true);
            $reason = $decoded['error'] ?? null;
        }

        $response = $this->responseFactory->createResponse(
            $info['http_code'],
            $reason,
            $headers,
            $result
        );

        return $response;
    }


    protected function setHeaders(ResourceInterface $resource, array $headers): void
    {
        if (!isset($headers['authorization'])) {
            switch ($this->authType) {
                case self::AUTH_NONE:
                    break;
                case self::AUTH_BASIC:
                    $headers[] = sprintf('authorization: basic %s:%s', $this->authData['login'],
                        $this->authData['password']);
                    break;
                case self::AUTH_TOKEN:
                    $headers[] = sprintf('authorization: %s %s', $this->authData['prefix'], $this->authData['token']);
                    break;
                default:
                    throw new Exception('Authorization type not supported. Client::AUTH_BASIC and Client::AUTH_TOKEN are only supported types.');
            }
        }

        $resource->setHeaders($headers);
    }
}